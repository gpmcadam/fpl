const Kafka = require('node-rdkafka');
const request = require('request-promise');

const INTERVAL_HOUR_1 = 1000*60*60;

console.log('starting fpl producer');

const producer = new Kafka.Producer({
    'metadata.broker.list': 'localhost:9092'
});

const send = (topic, message, key) => {
    console.log(`Producing: ${JSON.stringify({ topic, key })}`)
    return producer.produce(
        topic,
        null,
        new Buffer(message),
        key,
        Date.now()
    );
};

const getPlayers = () => {
    console.log('Fetching data');
    return request({
        uri: 'https://fantasy.premierleague.com/drf/bootstrap-static',
        json: true
    })
    .then(res => res.elements);
};

const produce = () => {
    getPlayers().then(players => {
        console.log('Sending to kafka on topic: fpl-players');
        players.map(player => {
            player.timestamp = Date.now();
            console.log(`Sending player: ${player.id}`);
            send('fpl-players', JSON.stringify(player), `player-${player.id}`);
        });
    })
    .catch(err => {
        console.error('A problem occurred when sending our message');
        console.error(err);
    });
}

producer.on('ready', function() {
    console.log('ready!');
    produce();
    // setInterval(produce, INTERVAL_HOUR_1);
});

producer.on('error', function(e) {
    console.error('There was a problem');
    console.error(e);
});

producer.connect();
